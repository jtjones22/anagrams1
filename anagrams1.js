function alphabetize(a) {
    return a.toLowerCase().split("").sort().join("").trim();
}

function getAnagramsOf(input){
    output = []
    input = alphabetize(input)
    for (i = 0; i < words.length; i++){
        if (alphabetize(words[i])===input){
            output.push(words[i])
        }
    }
    return output
}

let button = document.getElementById("findButton");

button.onclick = function () {
    
    let typedText = document.getElementById("input").value;
    let text = getAnagramsOf(typedText).join(' / ');
    let span = document.createElement("span");
    let textContent = document.createTextNode(text);
    span.appendChild(textContent);
    document.body.appendChild(span);
}